import PrevisionAlgo as prevision
import datetime;
import base64
import pandas as pd
from decouple import Config,RepositoryEnv
import requests
import json

def getPrevision(idUser,idServer):
    error_message = isValidServerId(idUser,idServer)
    if error_message != "":
        return error_message,400

    result = getPreviousNbPlayersServer(idServer)
    if result[1] != 200:
        return result
    df = pd.json_normalize(result[0])
    df = df.set_index('timestamp')
    df = df.rename(columns={'value':'count'})
    df = df.astype({"count":int})

    df = prevision.getPrediction(df)
    df = df.reset_index().rename(columns={'date':'timestamp'})
    return json.dumps(json.loads(df.to_json(orient='records')), indent=2)

def isValidServerId(idUser,idServer):
    env_config = Config(RepositoryEnv('./.env'))
    response = requests.get(env_config.get('URL_SERVER_SERVICE')+"/"+str(idServer))
    if response:
        data_return = response.json()
        idUserRequested = data_return['idUser']
        if str(idUser) == str(idUserRequested):
            return ""
        return "The sever id :"+str(idServer) +"doesn't belong to this idUser:"+str(idUser)
    return response.text
        
def getPreviousNbPlayersServer(idServer):
    env_config = Config(RepositoryEnv('./.env'))
    end = round(datetime.datetime.now().timestamp())
    start = round((datetime.datetime.now() - datetime.timedelta(15)).timestamp())
    url = env_config.get('URL_METRICS_SERVICE')+"/"+str(idServer)+"/players?start="+str(start)+"&end="+str(end)+"&offset=5&unit=m"
    response = requests.get(url,headers=getHeaderRequests())
    if (response):
        data_returned = response.json()
        if len(data_returned) > 0:
            return data_returned,200
        return "Cannot get metrics, maybe too measurements ?",500
    return response.text,500
    
def getHeaderRequests():
    env_config = Config(RepositoryEnv('./.env'))
    password = env_config.get('INTERNAL_BASIC_AUTH_TOKEN')
    passwordBase64 = base64.b64encode(password.encode('ascii'))
    header = {'Authorization':'Basic '+passwordBase64.decode('ascii')}
    return header