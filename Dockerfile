FROM tiangolo/uwsgi-nginx-flask:python3.8

RUN pip install pipenv

WORKDIR /opt/app

COPY Pipfile* /opt

RUN pipenv lock --requirements > /opt/requirements.txt

RUN pip install -r /opt/requirements.txt

COPY . .

ENV LISTEN_PORT 8080
EXPOSE 8080
