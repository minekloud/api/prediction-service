from flask import Flask, request, jsonify
import PredictionService as service
import base64
import json
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/api/prediction/<idServer>')
@cross_origin()
def getPrevision(idServer):
    auth = request.headers['Authorization'].split('.')[1]
    auth = base64.b64decode(auth + '===').decode('ascii')
    idUser = json.loads(auth)["sub"]
    if not idUser or not idServer: 
        return "IdUser or idServer not define",400
    return service.getPrevision(idUser,idServer)