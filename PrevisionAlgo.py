import warnings  # `do not disturbe` mode
from datetime import datetime, timedelta
import numpy as np  # vectors and matrices
import pandas as pd  # tables and data manipulations
from fbprophet import Prophet
import json
from fbprophet.serialize import model_to_json, model_from_json

warnings.filterwarnings('ignore')

def getPrediction(df):
    df.index = pd.to_datetime(df.index, unit='s')
    df = df["count"].resample("H").apply([np.mean])
    df = df.dropna()
    print("nb data:"+str(len(df.index)))
    if 24 <= len(df.index) < 100:
        print("Based on other servers data")
        return predictBasedOtherServersData(df)
    elif len(df.index) > 100:
        print("based on server data")
        return predict(df)
    else:
        return pd.DataFrame()


def predict_change():

    with open('serialized_model.json', 'r') as fin:
        m = model_from_json(json.load(fin))  # Load model

    # Prediction
    future = m.make_future_dataframe(periods=2000, freq='h')
    forecast = m.predict(future)
    forecast[['ds', 'yhat']]
    #m.plot(forecast)

    # Get results in new dataset
    final_df = pd.DataFrame()
    final_df['date'] = forecast['ds']
    final_df['change'] = forecast['yhat']
    final_df.index = final_df['date']
    final_df.pop('date')
    return final_df

def predict(df):

    # Clean dataframe
    df['y'] = df["mean"]
    df['ds'] = df.index

    # Define model
    m = Prophet()
    m.fit(df)

    # Prediction
    future = m.make_future_dataframe(periods=2000, freq='h')
    forecast = m.predict(future)
    forecast[['ds', 'yhat']]
    m.plot(forecast)

    # Get results in new dataset
    final_df = pd.DataFrame()
    final_df['date'] = forecast['ds']
    final_df['mean'] = forecast['yhat']
    final_df.index = final_df['date']
    final_df.pop('date')
    final_df = getOnlyFistTwoWeeksPrediction(final_df)
    return final_df


def predictBasedOtherServersData(df_server):
    df = predict_change()
    df_prediction = predictionOneServerPlayer(df_server,df)
    return df_prediction


def predictionOneServerPlayer(server_df, global_prediction_df):
    server_hour_df = server_df.groupby(server_df.index.hour).mean()
    df_prediction = getOnlyFistTwoWeeksPrediction(global_prediction_df)
    df_prediction['nbPlayer'] = df_prediction['change']
    for i in range(0, len(df_prediction)):
        hour = df_prediction.index[i].hour
        if i < 24:
            nbPlayer = server_hour_df.loc[server_hour_df.index == hour]['mean']
        if df_prediction["change"][i] >= 0:
            df_prediction["nbPlayer"][i] = round(nbPlayer + (nbPlayer * abs(df_prediction["change"][i])))
        else:
            df_prediction["nbPlayer"][i] = round(nbPlayer - (nbPlayer * abs(df_prediction["change"][i])))
    df_prediction.pop('change')

    return df_prediction

def getOnlyFistTwoWeeksPrediction(df):
    df = df[df.index >= datetime.now()]
    df = df[df.index <= (datetime.now() + timedelta(15))]
    return df
